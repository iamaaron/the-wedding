<?php
    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

      $validationError = "";

      $servername = "internal-db.s171463.gridserver.com";
      $username = "db171463_wedd";
      $password = "Jessica123.";
      $dbname = "db171463_testwedding";

      // Create connection
      $conn = new mysqli($servername, $username, $password, $dbname);
      // Check connection
      if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
      }

      function validateForm() {
        if (empty($fullName)) {
          $validationError = "Please enter a name.";
          return false;
        } else if (empty($phoneNumber)) {
          $validationError = "Please enter a phone number in which we may be able to reach you.";
          return false;
        } else if (empty($address)) {
          $validationError = "Please enter an address.";
          return false;
        } else if (empty($attending)) {
          $validationError = "Please specify if you are attending or not.";
          return false;
        } else if (empty($plusone)) {
          $validationError = "Please specify if you are bringing a guest.";
          return false;
        }
        return true;
      }

        // Get the form fields and remove whitespace.
        $fullName = $conn->real_escape_string($_POST["fullName"]);
        $phoneNumber = $conn->real_escape_string($_POST["phoneNumber"]);
        $email = $conn->real_escape_string(trim(filter_var($_POST["email"]), FILTER_SANITIZE_EMAIL));
        $address = $conn->real_escape_string($_POST["address"]);
        $attending = $conn->real_escape_string($_POST["attending"]);
        $plusOne = $conn->real_escape_string($_POST["plusone"]);
        $plusOneName = $conn->real_escape_string($_POST["plusonename"]);
        $familyMemberOneName = $conn->real_escape_string($_POST["familymember1name"]);
        $familyMemberTwoNname = $conn->real_escape_string($_POST["familymember2name"]);
        $familyMemberThreeNname = $conn->real_escape_string($_POST["familymember3name"]);
        $rsvpDate = date('Y-m-d');

        // Check that data was sent to the mailer.
        if (!validateForm) {
            // Set a 400 (bad request) response code and exit.
            http_response_code(400);
            echo $validationError;
            exit;
        }

        $sql = "INSERT INTO guests_finalized_test (fullname, phonenumber, address, email, attending, plusone, plusonename, familymember1name, familymember2name, familymember3name, rsvpdate)
        VALUES ('$fullName', '$phoneNumber', '$address', '$email', '$attending', '$plusOne', '$plusOneName',
                '$familyMemberOneName', '$familyMemberTwoNname', '$familyMemberThreeNname', '$rsvpDate')";

        if ($conn->query($sql) === TRUE) {
            // Set a 200 (okay) response code.
            http_response_code(200);
            echo "Thank you for your RSVP. It has been submitted. We will contact you if we need more information.";
        } else {
            // Set a 500 (internal server error) response code.
            http_response_code(500);
            echo "Oops! Something went wrong and we could not accept your RSVP submission. Please try again or contact Aaron at aaron@iamaaron.com.";
        }

        $conn->close();

    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "Oops! Something went wrong and we could not accept your RSVP submission. Please try again or contact Aaron at aaron@iamaaron.com.";
    }
?>
