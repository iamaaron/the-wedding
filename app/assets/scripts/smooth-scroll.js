jQuery(document).ready(function() {
  jQuery('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        if (jQuery(window).width() <= 1024) {
          jQuery('html,body').animate({
            scrollTop: target.offset().top - 75
          }, 1000);
          return false;
        } else {
          jQuery('html,body').animate({
            scrollTop: target.offset().top - 100
          }, 1000);
          return false;
        }
      }
    }
  });
});
