// Mobile navigation.
jQuery(document).ready(function() {
  jQuery('#menu').click(function () {
    jQuery('.container').hide();
    jQuery('#mobile-navigation').show();
  });
  jQuery('#exit').click(function () {
    jQuery('.container').show();
    jQuery('#mobile-navigation').hide();
  });
  jQuery('#mobile-navigation a').click(function () {
    jQuery('.container').show();
    jQuery('#mobile-navigation').hide();
  });
});
