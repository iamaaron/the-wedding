jQuery(document).ready(function() {

  // Variable declarations.
  var form = jQuery('#rsvp');
  var formMessages = jQuery('#rsvp-messages');

  // Clears plus one values and hides form field.
  function clearPlusOne() {
    jQuery('.plus-one').hide();
    jQuery('#plusonename').val('');
  }

  function resetPlusOneControl() {
    jQuery('.plus-one-control').show();
  }

  // Reset select element to 'No Plus One'.
  function resetPlusOne() {
    jQuery("#plusone option:first").attr('selected','selected');
  }

  // Clears family member form fields and hides wrapping element.
  function clearFamilyMembers() {
    jQuery('.family-member').hide();
    jQuery('#familymember1name').val('');
    jQuery('#familymember2name').val('');
    jQuery('#familymember3name').val('');
  }

  // Resets family member fields to show them, on reinitialization.
  function resetFamilyMembers() {
    jQuery(jQuery('.addFamilyMember')[0]).show();
    jQuery(jQuery('.addFamilyMember')[1]).show();
  }

  // Shows family member fields, sequentially.
  function showNextFamilyMemberField(position) {
    jQuery(jQuery('.family-member .addFamilyMember')[position]).hide();
    jQuery(jQuery('.family-member')[position + 1]).show();
  }

  // Updates family member position and passes it into showNextFamilyMemberField.
  jQuery('.addFamilyMember').click(function (event) {
    var familyMemberPosition = jQuery(this).data('member');
	  event.preventDefault();
    showNextFamilyMemberField(familyMemberPosition);
  });

  // Listen for changes to the attending select element.
  jQuery('#attending').change(function () {
    if (jQuery(this).val() === 'Yes') {
      jQuery('.plus-one-control').show();
    } else {
      jQuery('.plus-one-control').hide();
      clearPlusOne();
      clearFamilyMembers();
      resetPlusOne();
    }
  });

  // Listens for change to plus one select field and applies form transformations accordingly.
  jQuery('#plusone').change(function () {
    if (jQuery(this).val() == 'Plus One') {
      jQuery('.plus-one').show();
      clearFamilyMembers();
    } else if (jQuery(this).val() == 'Family Member') {
      jQuery(jQuery('.family-member')[0]).show();
      resetFamilyMembers();
      clearPlusOne();
    } else {
      clearFamilyMembers();
      clearPlusOne();
    }
  });

  // Set up an event listener for the contact form.
  jQuery(form).submit(function (event) {
    // Stop the browser from submitting the form.
    event.preventDefault();

    // Serialize the form data.
    var formData = jQuery(form).serialize();

    // Submit the form using AJAX.
    jQuery.ajax({
      type: 'POST',
      url: jQuery(form).attr('action'),
      data: formData
    }).done(function (response) {

      // Make sure that the formMessages div has the 'success' class.
      jQuery(formMessages).removeClass('error');
      jQuery(formMessages).addClass('success');

      // Clear the form and reset all fields.
      form[0].reset();
      clearPlusOne();
      clearFamilyMembers();
      resetFamilyMembers();
      resetPlusOne();
      resetPlusOneControl();

      // Set the message text.
      jQuery(formMessages).text(response);

    }).fail(function (data) {

      // Make sure that the formMessages div has the 'error' class.
      jQuery(formMessages).removeClass('success');
      jQuery(formMessages).addClass('error');

      // Set the message text.
      if (data.responseText !== '') {
        jQuery(formMessages).text(data.responseText);
      } else {
        jQuery(formMessages).text('Oops! An error occured and your message could not be sent. ' +
                             'Please review the form and try again or e-mail Aaron at aaron@iamaaron.com.');
      }
    });
  });
});
