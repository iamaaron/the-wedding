/*global require*/
(function (r) {
    'use strict';

  // Requiring Gulp and all of it's dependencies (plug-ins).
  var gulp = require('gulp'),
      uglify = require('gulp-uglify'),
      less = require('gulp-less'),
      plumber = require('gulp-plumber'),
      autoprefixer = require('gulp-autoprefixer'),
      connect = require('gulp-connect'),
      imagemin = require('gulp-imagemin'),
      gutil = require('gulp-util'),
      del = require('del'),
      minifyHTML = require('gulp-minify-html'),
      concat = require('gulp-concat'),
      fileInclude = require('gulp-file-include');

  // Defining local web server task.
  gulp.task('connect', function(done) {
    connect.server({
      root: 'build',
      livereload: true
    });
    done();
  });

  // Defining HTML livereload task.
  gulp.task('html', function (done) {
    var options = {
      conditionals: true,
      spare: true,
      empty: true,
      cdata: true
    };

    gulp.src('app/index.html')
        .pipe(fileInclude({
          prefix: '@@',
          basepath: '@file'
        }))
        .pipe(minifyHTML(options))
        .pipe(gulp.dest('build/'))
        .pipe(connect.reload());
    done();
  });

  gulp.task('php', function (done) {
    gulp.src('app/**/*.php')
        .pipe(gulp.dest('build/'))
        .pipe(connect.reload());
    done();
  });

  gulp.task('essentials', function (done) {
    gulp.src('app/essentials/**/*')
        .pipe(gulp.dest('build/'))
        .pipe(connect.reload());
    done();
  });

  // Defining Image Compression task.
  gulp.task('images', function(done){
    gulp.src('app/assets/images/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('build/images'))
        .pipe(connect.reload());
    done();
  });

  // Defining JavaScript Uglify and Error catching (Plumber) task.
  gulp.task('scripts', function(done){
    gulp.src('app/assets/scripts/**/*.js')
        .pipe(plumber())
        .pipe(uglify())
        .pipe(concat('app.js'))
        .pipe(gulp.dest('build/scripts'))
        .pipe(connect.reload());
    done();
  });

  // Defining LESS compilation, autoprefixer, compression task.
  gulp.task('styles', function(done){
    gulp.src('app/assets/styles/main.less')
        .pipe(less({compress: true}).on('error', gutil.log))
        .pipe(autoprefixer('last 10 versions'))
        .pipe(gulp.dest('build/styles'))
        .pipe(connect.reload());
    done();
  });

  // Clean task, do not use gulp-clean or gulp-rimraf. This returns a promise.
  gulp.task('clean', function (done) {
	  del(['build/**/*']).then(function (){
      done();
    });
  });

  // Defining Watch task.
  gulp.task('watch', function(done){
    gulp.watch('app/**/*.js', gulp.series('scripts'));
    gulp.watch('app/assets/images/*', gulp.series('images'));
    gulp.watch('app/**/*.less', gulp.series('styles'));
    gulp.watch('app/**/*.php', gulp.series('php'));
    gulp.watch('app/**/*.html', gulp.series('html'));
    done();
  });

  // Default task to run.
  gulp.task('default', gulp.series('clean', 'html', 'php', 'essentials', 'scripts', 'styles', 'images', 'connect',
                                   'watch'));

}(require));
